package com.holgergarcia.shopCheckout.items

import groovy.transform.Canonical

@Canonical
class SpecialPrice {
    Integer specialPrice
    Integer unitCount

    Integer getTotalSpecialPrice(Integer count) {
        specialPrice.toInteger() * (count / this.unitCount).toInteger()
    }

    Integer getRemainingItemCount(Integer count) {
        (count % this.unitCount).toInteger()
    }
}
