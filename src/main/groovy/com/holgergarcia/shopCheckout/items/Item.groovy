package com.holgergarcia.shopCheckout.items

import groovy.transform.Canonical

@Canonical
class Item {
    String stockKeepingUnit
}
