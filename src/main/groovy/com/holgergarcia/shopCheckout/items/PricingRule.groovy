package com.holgergarcia.shopCheckout.items

import groovy.transform.Canonical

@Canonical
class PricingRule {

    Item item
    Integer unitPrice
    SpecialPrice specialPrice

    // TODO: cleanup and refactor this calculation
    Integer getTotalPrice(Integer count) {
        Integer price = 0
        if (this.specialPrice) {
            price += this.specialPrice.getTotalSpecialPrice(count)
            price += this.unitPrice.toInteger() * this.specialPrice.getRemainingItemCount(count)
        } else {
            price += this.unitPrice.toInteger() * count
        }
        price
    }
}
