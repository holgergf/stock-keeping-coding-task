package com.holgergarcia.shopCheckout

import com.holgergarcia.shopCheckout.items.Item
import com.holgergarcia.shopCheckout.items.PricingRule

class CheckOut {

    private final Set<PricingRule> pricingRules
    private List<Item> scannedItems = []

    CheckOut(Set<PricingRule> pricingRules) {
        this.pricingRules = pricingRules
    }

    void scan(Item item) {
        // TODO: Validate item exists in our pricing rule list
        scannedItems << item
    }

    void "void"(Item item) {
        // TODO: Validate that an item with this SKU actually exists
        scannedItems.remove(item)
    }

    Integer total() {
        pricingRules.sum { PricingRule pricingRule ->
            Integer itemCount = (Integer) scannedItems.count { Item item ->
                pricingRule.item == item
            }
            pricingRule.getTotalPrice(itemCount)
        } as Integer
    }
}
