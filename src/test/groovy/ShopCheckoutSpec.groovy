import com.holgergarcia.shopCheckout.CheckOut
import com.holgergarcia.shopCheckout.items.Item
import com.holgergarcia.shopCheckout.items.PricingRule
import com.holgergarcia.shopCheckout.items.SpecialPrice
import spock.lang.Shared
import spock.lang.Specification

class ShopCheckoutSpec extends Specification {

    @Shared
    Set<PricingRule> pricingRules = []

    @Shared
    Item itemA = new Item(stockKeepingUnit: 'A')
    @Shared
    Item itemB = new Item(stockKeepingUnit: 'B')
    @Shared
    Item itemC = new Item(stockKeepingUnit: 'C')

    def setup() {
        pricingRules << new PricingRule(item: itemA, unitPrice: 50, specialPrice: new SpecialPrice(specialPrice: 130, unitCount: 3))
        pricingRules << new PricingRule(item: itemB, unitPrice: 30, specialPrice: new SpecialPrice(specialPrice: 45, unitCount: 2))
        pricingRules << new PricingRule(item: itemC, unitPrice: 20, specialPrice: null)
    }

    void "We can scan a single item and the total is correct"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        checkOut.scan(item)

        then:
        checkOut.total() == total

        where:
        item  | total
        itemA | 50
        itemB | 30
        itemC | 20
    }

    void "We can scan and remove multiple single items and the total is correct"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        checkOut.scan(itemA)
        checkOut.scan(itemB)

        then:
        checkOut.total() == 80

        when:
        checkOut.void(itemB)

        then:
        checkOut.total() == 50
    }

    void "We can apply a discount given a specific number of items"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        2.times {
            checkOut.scan(itemB)
        }

        then:
        checkOut.total() == 45
    }

    void "We can combine discounts and unit prices for an item"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        3.times {
            checkOut.scan(itemB)
        }

        then:
        checkOut.total() == 45 + 30
    }

    void "We can combine discounts and unit prices for multiple items"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        3.times {
            checkOut.scan(itemB)
        }
        checkOut.scan(itemA)

        then:
        checkOut.total() == 45 + 30 + 50
    }

    void "Discounts are removed when enough items are voided"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        2.times {
            checkOut.scan(itemB)
        }

        then:
        checkOut.total() == 45

        when:
        checkOut.void(itemB)

        then:
        checkOut.total() == 30
    }

    void "Price rules are not tied to the item objects used to create them"() {
        given:
        CheckOut checkOut = new CheckOut(pricingRules)

        when:
        checkOut.scan(new Item("B"))

        then:
        checkOut.total() == 30
    }
}
