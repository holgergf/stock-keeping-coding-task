# Holger Garcia Coding Exercise 

## Introduction

For this version I tried to stay close to the suggested time limit, that meant there
is some things that could use some clean up and refactor, detailed further into this document

## Usage
```bash
# To execute the tests
./gradlew test
```

## Left to implement
### Validation
  As the focus of the exercise was more a general feel of how I work(and you mention not dealing with edge cases), 
  I decided to ignore validation both in domain classes(empty Strings for names, non negative values for numbers) 
  and in the CheckOut class(you can add items that do not exist in the pricing rules and will be ignored, or remove items that weren't there to begin with)
  
### Pricing rule flexibility
For the moment I only implemented the proposed pricing rules, as SpecialPrice contains the internal logic 
to deal with offers it should be quick to create an interface with the required fields and methods and implement for example percentage based offers.

## Future work

* Code for the `total()` method could be improved and refactored
* Add validation
* Support for multiple special prices per Item
* General refactor






 